/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Timer;



/**
 *
 * @author Vicente
 */
public class Board extends javax.swing.JPanel {

    /*class MyMouseAdapter extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            int row = (int) e.getPoint().getX() / getWidth();
            int col = (int) e.getPoint().getY() / getHeight();
            pointClicked(row, col);
            repaint();

        }
    }*/
    private static final int NUM_ROWS = 40;
    private static final int NUM_COLS = 40;

    private Point[][] currentMap;
    private Point[][] nextMap;
    private Timer gameTimer;
    private int deltaTime;
    private List<Point> pointsAround;
    private int pointsAlive;
    
    private class MyMouseAdapter extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
                int row = (int) e.getY() / squareHeight();
                int col = (int) e.getX() / squareWidth();
                pointClicked(row, col);
                repaint();
            }
    }
    

    public Board() {
        super();
        myInit();
        
        addMouseListener(new MyMouseAdapter());

        /*addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                int row = (int) e.getY() / squareHeight();
                int col = (int) e.getX() / squareWidth();
                pointClicked(row, col);
                repaint();
            }

        });*/

        gameTimer = new Timer(deltaTime, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int row = 0; row < NUM_ROWS; row++) {
                    for (int col = 0; col < NUM_ROWS; col++) {
                        pointsAround = getPointsAround(row, col);
                        pointsAlive = 0;
                        for (Point p : pointsAround) {
                            if (p.getStatus()) {
                                pointsAlive++;
                            }
                        }
                        killOrRevivePoint(row, col);
                    }
                }
                for (int row = 0; row < NUM_ROWS; row++) {
                    for (int col = 0; col < NUM_ROWS; col++) {
                        currentMap[row][col] = nextMap[row][col];
                    }
                }
                repaint();
            }
        });
    }

    public void startTimer() {
        gameTimer.start();
    }

    public void pauseTimer() {
        gameTimer.stop();
    }

    private void killOrRevivePoint(int row, int col) {
        if(currentMap[row][col].getStatus()){
            if (pointsAlive != 2 && pointsAlive != 3){
                nextMap[row][col] = new Point(row, col, false);
            }else{
                nextMap[row][col] = new Point(row, col, true);
            }
        }else{
            if (pointsAlive == 3){
                nextMap[row][col] = new Point(row, col, true);
            }else{
               nextMap[row][col] = new Point(row, col, false); 
            }
        }
    }

    private List<Point> getPointsAround(int row, int col) {
        List<Point> pointList = new ArrayList<>();

        for (int currentRow = row - 1; currentRow <= row + 1; currentRow++) {
            for (int currentCol = col - 1; currentCol <= col + 1; currentCol++) {
                if (pointExist(currentRow, currentCol) && (currentCol != col || currentRow != row)) {
                    pointList.add(currentMap[currentRow][currentCol]);
                }
            }
        }
        return pointList;

    }

    private boolean pointExist(int currentRow, int currentCol) {
        if (currentRow < 0 || currentRow >= NUM_ROWS || currentCol < 0 || currentCol >= NUM_COLS) {
            return false;
        }
        return true;
    }

    private void pointClicked(int row, int col) {
        currentMap[row][col].setStatus(!currentMap[row][col].getStatus());
    }

    private void myInit() {
        currentMap = new Point[NUM_ROWS][NUM_COLS];
        nextMap = new Point[NUM_ROWS][NUM_COLS];
        deltaTime = 1000;

        for (int row = 0; row < NUM_ROWS; row++) {
            for (int col = 0; col < NUM_COLS; col++) {
                currentMap[row][col] = new Point(row, col, false);
                nextMap[row][col] = new Point(row, col, false);
            }
        }

        /*for (int row = 0; row < 5; row++) {
            for (int col = 0; col < 10; col += 2) {
                currentMap[row][col].setStatus(true);
            }
        }*/
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        for (int row = 0; row < NUM_ROWS; row++) {
            for (int col = 0; col < NUM_ROWS; col++) {
                Util.drawSquare(g2d, row, col, squareWidth(), squareHeight(), currentMap[row][col].getStatus());
            }
        }
    }

    private int squareWidth() {
        if (NUM_COLS != 0) {
            return getWidth() / NUM_COLS;
        }
        return getWidth() / 100;
    }

    private int squareHeight() {
        if (NUM_ROWS != 0) {
            return getHeight() / NUM_ROWS;
        }
        return getHeight() / 100;
    }

}
