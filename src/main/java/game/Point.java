/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 *
 * @author Vicente
 */
public class Point {

    private int row;
    private int col;
    private boolean status;

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public Point(int row, int col, boolean status) {
        this.row=row;
        this.col=col;
        this.status=status;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }   
}